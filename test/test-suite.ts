import { Suite } from "../src/Suite";

const suite = new Suite();
suite.addScenario("contacts", "Initial contacts are present", async (describe, faker) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      describe(faker.name.firstName());
      resolve();
    }, 500);
  });
});

export default suite;

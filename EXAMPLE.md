# Example usage of Copious in a TypeORM application

Say we have a `User` entity that we use in some login function. We might want some data
seeded so that a developer can log into application that they are working on.

The user logs in with their email address and a password, and also has a name
in this example.

## User.ts

```typescript
@Entity()
export default class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column()
  email: string;

  @Column()
  password: string;
}
```

## UserRecipe.ts

The User entity will have a recipe that Copious can use to construct new Users.
For convenience, we may include some helper functions that provide an easy to read
description of how the User will be constructed, eg, `withName()` and `withEmail()`

```typescript
export default class UserRecipe extends Recipe {
  fullName?: string;
  email?: string;
  password?: string;

  withName(fullName: string) {
    this.fullName = fullName;

    return this;
  }

  withEmail(email: string) {
    this.email = email;

    return this;
  }

  withPassword(password: string) {
    this.password = password;

    return this;
  }
}
```

## UserFactory.ts

The UserFactory has everything needed to create new Users, save them to the database
and return them for use elsewhere if required.

All sorts of functions can be created to provide convenience. This is a simple one,
but could be extended with functons such as `createAdminUser()`,
`createGoogleAccountUser()` etc.

This example also makes use of `faker` if the name is unset in the `UserRecipe`.

```typescript
import User from "./User";
import UserRecipe from "./UserRecipe";

export default class UserFactory extends EntityFactory<User> {
  // Must override this and return your `T`
  getEntity() {
    return User;
  }

  public async createUser(recipe?: UserRecipe): Promise<User> {
    let faker = this.getFaker();

    if (!recipe) {
      recipe = new UserRecipe();
    }

    if (!recipe.fullName) {
      recipe.fullName = faker.name.firstName() + " " + faker.name.lastName();
    }

    if (!recipe.email) {
      recipe.email = this.getFaker().internet.email(recipe.fullName, "", "gcdtech.com");
    }

    if (!recipe.password) {
      recipe.password = "password";
    }

    if (!recipe.role) {
      recipe.role = Role.None;
    }

    let user = await this.findOrCreateEntity({ email: recipe.email }, recipe);

    return user;
  }
}
```

### Performing operations on an entity before saving to the database.

If there are extra things you want to do on your User entity (such as hashing a password), you can set the commit flag to false then save the entity yourself.

```typescript
...
// Set third param to false so that this function doesn't commit to the db
let user = await this.findOrCreateEntity({ email: recipe.email }, recipe, false);

// Perform your other operations
user.hashPassword();

// Now we can commit.
await this.getRepository().save(user);
...
```

## authSuite.ts

Your authentication suite will set up a scenario such that a user will be able to log
in with the provided credentials.

```typescript
const authSuite = new Suite();

authSuite.addScenario("authentication", "Sign In", async (describe, faker) => {
  await new UserFactory().createUser(
    new UserRecipe()
      .withEmail("cwilkinson@gcdtech.com")
      .withPassword("password")
    );

  describe("Created a user with the email `cwilkinson@gcdtech.com` and the password `password`");
});

export default authSuite;
```

## indexSuite.ts

Your main composition suite will add `authSuite` and include it in its list of
scenarios during runtime.

```typescript
import authSuite from "./authSuite";

class MySeederSuite extends Suite {
  async run(): Promise<void> {
    EntityFactory.setConnection(await createConnection());

    return await super.run();
  }
}

const suite = new MySeederSuite();

suite.addSuite(authSuite);

export default suite;
```

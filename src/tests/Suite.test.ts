import { Suite } from "../Suite";

it("should instantiate without failure", () => {
  expect(new Suite()).not.toBeUndefined();
});

it("should add scenario to scenarios list when addScenario is called", () => {
  let suite = new Suite();
  let scenarioName = "my complex scenario";

  suite.addScenario("some scope", scenarioName, () => {});

  expect(suite.scenarios).toHaveLength(1);
});

it("should add scenario with name to scenarios list when addScenario is called", () => {
  let suite = new Suite();

  let scenarioName = "my complex scenario";

  suite.addScenario("some scope", scenarioName, () => {});

  expect(suite.scenarios[0].name).toEqual(scenarioName);
});

it("should add scenario with given seeder function to scenarios list when addScenario is called", () => {
  let suite = new Suite();
  let expectedSeederFunctionOutput = "Hello from the seeder function";
  let mySeederFunction = () => {
    throw expectedSeederFunctionOutput;
  };

  suite.addScenario("some scope", "some scenario", mySeederFunction);

  expect(suite.scenarios[0].execute).toThrow(expectedSeederFunctionOutput);
});

it("should add all of the scenarios from a given suite to this suite's scenario list when addSuite is called", () => {
  let subSuite = new Suite();
  subSuite.addScenario("some scope", "some scenario", () => {});

  let mainSuite = new Suite();
  mainSuite.addSuite(subSuite);

  expect(mainSuite.scenarios).toHaveLength(1);
});

it("should not replace this suite's list of scenarios when a sub suite is added", () => {
  let subSuite = new Suite();
  subSuite.addScenario("some scope", "some scenario", () => {});

  let mainSuite = new Suite();
  subSuite.addScenario("some other scope", "some other scenario", () => {});

  mainSuite.addSuite(subSuite);

  expect(mainSuite.scenarios).toHaveLength(2);
});

it("should run a scenario when run is called", async () => {
  let suite = new Suite();
  let myScenarioFunction = jest.fn();
  suite.addScenario("some scope", "some scenario", myScenarioFunction);

  await suite.run();

  expect(myScenarioFunction).toHaveBeenCalledTimes(1);
});

it("should run all scenarios when run is called", async () => {
  let suite = new Suite();
  let myScenarioFunction = jest.fn();
  suite.addScenario("some scope", "some scenario", myScenarioFunction);
  suite.addScenario("some scope", "some other scenario", myScenarioFunction);

  await suite.run();

  expect(myScenarioFunction).toHaveBeenCalledTimes(2);
});

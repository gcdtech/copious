export class Factory {
  protected static faker?: Faker.FakerStatic;

  public static setFaker(faker: Faker.FakerStatic) {
    Factory.faker = faker;
  }

  protected getFaker(): Faker.FakerStatic {
    return Factory.faker!;
  }
}

import FakerStatic = Faker.FakerStatic;

export interface IScenario {
  name: string;
  execute(describe: (message: string) => void, faker: FakerStatic): void;
}

import { Factory } from "./Factory";
import { Connection, Repository, FindOneOptions } from "typeorm";

export abstract class EntityFactory<T> extends Factory {
  private static connection: Connection;

  public static setConnection(connection: Connection) {
    EntityFactory.connection = connection;
  }

  public abstract getEntityConstructor(): new () => T;

  public getRepository(): Repository<T> {
    return EntityFactory.connection.getRepository(this.getEntityConstructor());
  }

  public async findOrCreateEntity(
    findOptions: Partial<T> | FindOneOptions<T>,
    recipe?: { [index: string]: any },
    commit: boolean = true
  ): Promise<T> {
    const constructor = this.getEntityConstructor();
    let existingItem = new constructor();
    const repository = this.getRepository();

    try {
      existingItem = await repository.findOneOrFail(findOptions);
    } catch (err) {}

    // Set (or reset) other values from the recipe before saving the entity
    if (recipe) {
      Object.assign(existingItem, recipe);
    }

    if (commit) {
      await repository.save(existingItem);
    }

    return existingItem;
  }
}
